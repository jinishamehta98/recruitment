<?php


namespace App\Jobs;


use App\Events\Event;
use App\Mail\RegisterEmailMail;
use Illuminate\Support\Facades\Mail;

class RegisterEmailJob extends Job
{

    /**
     * @var Event
     */
    public Event $event;

    /**
     * RegisterEmailJob constructor.
     *
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function handle(){
        Mail::send(new RegisterEmailMail($this->event));
    }
}