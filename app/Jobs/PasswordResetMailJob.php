<?php

namespace App\Jobs;

use App\Events\Event;
use App\Mail\SendPasswordResetMail;
use Illuminate\Support\Facades\Mail;

class PasswordResetMailJob extends Job
{
    /**
     * @var Event
     */
    public Event $event;

    /**
     * PasswordResetMailJob constructor.
     *
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function handle(){
        Mail::send(new SendPasswordResetMail($this->event));
    }
}