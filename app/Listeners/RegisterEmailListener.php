<?php

namespace App\Listeners;

use App\Events\RegisterEmailEvent;
use App\Jobs\RegisterEmailJob;
use Illuminate\Support\Carbon;

class RegisterEmailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Handle the event.
     *
     * @param RegisterEmailEvent $event
     *
     * @return void
     */
    public function handle(RegisterEmailEvent $event)
    {
        $job = (new RegisterEmailJob($event))->delay(
            Carbon::now()->addSeconds(10)
        );
        dispatch($job);
    }

}