<?php


namespace App\Listeners;

use \App\Events\SendPasswordResetMailEvent as Event;
use App\Jobs\PasswordResetMailJob;
use Carbon\Carbon;

class SendPasswordResetMailListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * @param Event $event
     */
    public function handle(Event $event)
    {
        $job = (new PasswordResetMailJob($event))->delay(
            Carbon::now()->addSeconds(10)
        );
        dispatch($job);
    }
}