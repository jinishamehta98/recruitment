<?php


namespace App\Repository\RolePermission;

use App\Models\Permission;
use App\Models\Role;
use App\Models\RolePermission;
use Illuminate\Database\Eloquent\Collection;

class RolePermissionRepository implements RolePermissionInterface
{

    /**
     * @var RolePermission
     */
    private RolePermission $model;

    public function __construct(RolePermission $model)
    {
        $this->model = $model;
    }

    public function assignPermissions(array $input): array
    {
        $rolePermission = [];
        foreach ($input['permissions'] as $permission_id) {

            $data = [
                'role_id' => $input['role_id'],
                'permission_id' => $permission_id,
            ];
            $rolePermission[] = $permission_id;
            $this->create($data);
        }

        $this->delete($input['role_id'],$rolePermission);
        return $rolePermission;
    }

    public function create(array $input): RolePermission
    {
        return $this->model->firstOrCreate($input);
    }

    public function delete($roleId,$input): bool
    {
        return $this->model
            ->where(['role_id' => $roleId])
            ->whereNotIn('permission_id',$input)
            ->delete();
    }

    public function getPermission(int $roleId): Collection
    {
        return $this->model
            ->where('role_id',$roleId)
            ->get();
    }
}