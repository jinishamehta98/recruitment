<?php


namespace App\Repository\RolePermission;


use App\Models\RolePermission;
use Illuminate\Database\Eloquent\Collection;

interface RolePermissionInterface
{
    /**
     * @param array $input
     *
     * @return array
     */
    public function assignPermissions(array $input): array;

    /**
     * @param array $input
     *
     * @return RolePermission
     */
    public function create(array $input): RolePermission;

    /**
     * @param int $roleId
     *
     * @return Collection
     */
    public function getPermission(int $roleId): Collection;

    /**
     * @param $roleId
     * @param $input
     *
     * @return bool
     */
    public function delete($roleId,$input): bool;
}