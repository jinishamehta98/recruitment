<?php

namespace App\Repository\Role;

use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;

interface RoleInterface
{
    /**
     * @return Collection
     */
    public function listRole(): Collection;

    /**
     * @param array $input
     *
     * @return Role
     */
    public function create(array $input): Role;

    /**
     * @param string $role
     *
     * @return bool
     */
    public function delete(string $role): bool ;

    /**
     * @param array $input
     *
     * @return array
     */
    public function createBulk(array $input): array;
}