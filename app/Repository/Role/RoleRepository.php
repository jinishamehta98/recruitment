<?php


namespace App\Repository\Role;


use App\Models\Role;
use Illuminate\Database\Eloquent\Collection;

class RoleRepository implements RoleInterface
{

    /**
     * @var Role
     */
    private $model;

    public function __construct(Role $model)
    {
        $this->model = $model;
    }

    /**
     * @inheritDoc
     */
    public function listRole(): Collection
    {
        return $this->model->all();
    }

    /**
     * @inheritDoc
     */
    public function create(array $input): Role
    {
        return $this->model->create($input);
    }

    /**
     * @inheritDoc
     */
    public function delete(string $role): bool
    {
        return $this->model
            ->where('role',$role)
            ->delete();
    }

    /**
     * @inheritDoc
     */
    public function createBulk(array $input): array
    {
        foreach($input['roles'] as $role){
            $roles[] = $this->create($role);
        }
        return $roles;
    }
}