<?php


namespace App\Repository\Permission;


use App\Models\Permission;
use Illuminate\Database\Eloquent\Collection;

class PermissionRepository implements PermissionInterface
{
    /**
     * @var Permission
     */
    private Permission $model;

    public function __construct(Permission $permission)
    {
        $this->model = $permission;
    }

    /**
     *{@inheritDoc}
     */
    public function listAll(): Collection
    {
        return $this->model->all();
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input): Permission
    {
        return $this->model->firstOrCreate($input);
    }

    /**
     * {@inheritDoc}
     */
    public function deletePermission(string $module = '', array $permission = []): bool
    {
        return $this->model
            ->where(['module' => $module])
            ->whereNotIn('permission', $permission ?? [])
            ->delete();
    }

    /**
     * {@inheritDoc}
     */
    public function deleteModule(array $modules = []): bool
    {
        return $this->model
            ->whereNotIn('module', $modules ?? [])
            ->delete();
    }

    /**
     * {@inheritDoc}
     */
    public function getId(array $permissions): array
    {
        return $this->model
            ->whereIn('permission', $permissions)
            ->pluck('id')
            ->toArray();
    }
}