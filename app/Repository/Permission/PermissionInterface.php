<?php

namespace App\Repository\Permission;

use App\Models\Permission;
use Illuminate\Database\Eloquent\Collection;

Interface PermissionInterface
{
    /**
     * @return Collection
     */
    public function listAll(): Collection;

    /**
     * @param array $input
     *
     * @return Permission
     */
    public function create(array $input): Permission;

    /**
     * @param string $module
     * @param array  $permission
     *
     * @return bool
     */
    public function deletePermission(string $module, array $permission): bool;

    /**
     * @param array $modules
     *
     * @return bool
     */
    public function deleteModule(array $modules): bool;

    /**
     * @param array $permissions
     *
     * @return array
     */
    public function getId(array $permissions): array;
}