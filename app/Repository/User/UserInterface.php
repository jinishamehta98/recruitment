<?php

namespace App\Repository\User;

use App\Models\User;
use Illuminate\Database\Eloquent\Collection;

interface UserInterface
{
    /**
     * @param array $input
     *
     * @return User
     */
    public function register(array $input): User;

    /**
     * @param array $input
     * @param int   $userId
     *
     * @return User
     */
    public function update(array $input, int $userId): User;

    /**
     * @param array $input
     * @param int   $userId
     *
     * @return mixed
     */
    public function setProfilePic(array &$input,int $userId);

    /**
     * @param int $userId
     *
     * @return User
     */
    public function view(int $userId): User;

    /**
     * @param array $input
     *
     * @return User
     */
    public function create(array $input): User;

    /**
     * @param int $userId
     *
     * @return bool
     */
    public function delete(int $userId): bool;

    /**
     * @return Collection
     */
    public function listUsers(): Collection;

    /**
     * @param array $input
     *
     * @return User
     */
    public function changePassword(array $input): User;

}