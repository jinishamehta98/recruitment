<?php

namespace App\Repository\User;

use App\Events\RegisterEmailEvent;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Auth;

class UserRepository implements UserInterface
{
    /**
     * @var User
     */
    protected User $model;

    /**
     * UserRepository constructor.
     *
     * @param User $model
     */
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * {@inheritDoc}
     */
    public function register(array $input): User
    {
        $user = $this->model->create($input);
        event(new RegisterEmailEvent($user));
        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function update(array $input, int $userId): User
    {
        // TODO: Implement update() method.
        $user = $this->model->findOrFail($userId);
        $this->setProfilePic($input, $userId);
        $user->update($input);
        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function setProfilePic(array &$input, int $userId)
    {
        // TODO: Implement setProfilePic() method.
        if (empty($input['profile_pic'])) {
            return;
        }
        $profilePic = $input['profile_pic'];
        $destPath = config('constants.image.profile_path');
        $input['profile_pic'] = $userId . '_' . time() . '.' . $profilePic->getClientOriginalExtension();
        $profilePic->storeAs($destPath, $input['profile_pic']);
    }

    /**
     * {@inheritDoc}
     */
    public function view(int $userId): User
    {
        return $this->model->findOrFail($userId);
    }

    /**
     * {@inheritDoc}
     */
    public function create(array $input): User
    {
        if (!isset($input['password'])) {
            $input['password'] = generatePassword();
        }
        $user = $this->model->create($input);
//        event(new CreateUserEmailEvent($user, $input['password']));
        return $user;
    }

    /**
     * {@inheritDoc}
     */
    public function delete(int $userId): bool
    {
        return $this->model
            ->findOrFail($userId)
            ->delete();
    }

    /**
     * {@inheritDoc}
     */
    public function listUsers(): Collection
    {
        return $this->model->all();
    }

    /**
     * {@inheritDoc}
     */
    public function changePassword(array $input): User
    {
        $id = Auth::user()->id;
        $user = $this->model->findOrFail($id);

        if(!password_verify($input['oldPassword'],$user->password)){
            return $this->model;
        }

        $user->update([
            'password' => $input['newPassword']
        ]);

        return $user->refresh();
    }
}