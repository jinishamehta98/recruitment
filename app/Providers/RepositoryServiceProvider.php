<?php


namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind(
            'App\Repository\User\UserInterface',
            'App\Repository\User\UserRepository'
        );

        $this->app->bind(
            'App\Repository\Role\RoleInterface',
            'App\Repository\Role\RoleRepository'
        );

        $this->app->bind(
            'App\Repository\Permission\PermissionInterface',
            'App\Repository\Permission\PermissionRepository'
        );

        $this->app->bind(
            'App\Repository\RolePermission\RolePermissionInterface',
            'App\Repository\RolePermission\RolePermissionRepository'
        );
    }
}