<?php

namespace App\Models;

use App\Events\SendPasswordResetMailEvent;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Support\Facades\Auth;
use Laravel\Lumen\Auth\Authorizable;
use Laravel\Passport\HasApiTokens;

class User extends Model implements AuthenticatableContract, AuthorizableContract, CanResetPasswordContract
{
    use Authenticatable, Authorizable, HasFactory,
        HasApiTokens, CanResetPassword;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'username',
        'email',
        'contact_no',
        'password',
        'role_id',
        'profile_pic',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /**
     * @return AuthenticatableContract|null
     */
    public static function authLogout()
    {
        $loggedInUser = Auth::user();
        $token = $loggedInUser->token();
        $token->revoke();
        return $loggedInUser;
    }

    /**
     * @param $value
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = app('hash')->make($value);
    }

    /**
     * @return HasManyThrough
     */
    public function permissions()
    {
        return $this->hasManyThrough(
            Permission::class,
            RolePermission::class,
            'role_id',
            'id',
            'role_id',
            'permission_id'
        );
    }

    /**
     * @return mixed
     */
    public function getAllPermissions()
    {
        $userPermissions = $this->permissions;
        $userPermissions = $userPermissions->map(function ($permission) {
            return ['name' => $permission['permission']];
        });
        return $userPermissions;
    }

    /**
     * @param string $token
     */
    public function sendPasswordResetNotification($token)
    {
        event(new SendPasswordResetMailEvent($this, $token));
    }
}
