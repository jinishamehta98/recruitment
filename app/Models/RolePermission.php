<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class RolePermission extends Model
{
   protected $fillable = [
     'role_id','permission_id'
   ];

    /**
     * @return BelongsTo
     */
    public function role(){
        return $this->belongsTo(Role::class,'id','role_id');
    }

    /**
     * @return HasOne|mixed
     */
    public function permission(){
        return $this->hasOne(Permission::class,'id','permission_id');
    }
}