<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Repository\User\UserInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * @var UserInterface
     */
    private UserInterface $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserInterface $repository
     */
    public function __construct(UserInterface $repository)
    {
        $this->userRepository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function listUser(){
        return $this->httpOK([
            'data' => [
                'users' => $this->userRepository->listUsers()
            ]
        ]);
    }

    /**
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function viewUser(int $userId){
        return $this->httpOK([
            'data' => [
                'user' => $this->userRepository->view($userId)
            ]
        ]);
    }

    /**
     * @param Request $request
     * @param int     $userId
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function updateUser(Request $request,int $userId){

        $input = $this->validate($request,[
            'first_name' => 'sometimes|max:255',
            'last_name' => 'sometimes|max:255',
            'contact_no' => 'sometimes|regex:/[0-9]{10}/',
            'role_id' => 'sometimes|exists:roles,id'
        ]);

        return $this->httpOK([
            'data' => [
                'user' => $this->userRepository->update($input,$userId)
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createUser(Request $request){

        $input = $this->validate($request,[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email',
            'contact_no' => 'required|regex:/[0-9]{10}/',
            'role_id' => 'required|exists:roles,id'
        ]);

        return $this->httpOK([
            'data' => [
                'user' => $this->userRepository->create($input)
            ]
        ]);
    }

    /**
     * @param int $userId
     *
     * @return JsonResponse
     */
    public function deleteUser(int $userId){
        return $this->httpOK([
            'data' => [
                'message' => 'deleted : '. $this->userRepository->delete($userId)
            ]
        ]);
    }
}