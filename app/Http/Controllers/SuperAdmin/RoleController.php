<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Repository\Role\RoleInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class RoleController extends Controller
{
    private RoleInterface $roleRepository;

    /**
     * RoleController constructor.
     *
     * @param RoleInterface $repository
     */
    public function __construct(RoleInterface $repository)
    {
        $this->roleRepository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function listRole()
    {
        return $this->httpOK([
            'data' => [
                'roles' => $this->roleRepository->listRole()
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createRole(Request $request)
    {
        $input = $this->validate($request, [
            'role' => 'required|unique:roles',
            'description' => 'sometimes|text',
        ]);

        return $this->httpOK([
            'data' => [
                'role' => $this->roleRepository->create($input),
            ],
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function deleteRole(Request $request)
    {
        $input = $this->validate($request, [
            'role' => 'required',
            'description' => 'sometimes|text',
        ]);

        return $this->httpOK([
            'data' => [
                'role' => $this->roleRepository->delete($input['role']),
            ],
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createRoles(Request $request)
    {
        $input = $this->validate($request, [
            'roles.*.role' => 'required|unique:roles',
            'roles.*.description' => 'sometimes|text',
        ]);

        return $this->httpOK([
            'data' => [
                'role' => $this->roleRepository->createBulk($input),
            ],
        ]);
    }
}