<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Repository\Permission\PermissionInterface;
use App\Repository\RolePermission\RolePermissionInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class RolePermissionController extends Controller
{

    /**
     * @var PermissionInterface
     */
    private PermissionInterface $permissionRepository;
    /**
     * @var RolePermissionInterface
     */
    private RolePermissionInterface $rolePermissionRepository;

    /**
     * RolePermissionController constructor.
     *
     * @param RolePermissionInterface $rolePermissionRepository
     * @param PermissionInterface     $permissionRepository
     */
    public function __construct(RolePermissionInterface $rolePermissionRepository,PermissionInterface $permissionRepository)
    {
        $this->rolePermissionRepository = $rolePermissionRepository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function assignPermissionToRole(Request $request){

        $input = $this->validate($request,[
            'role_id' => 'required|exists:roles,id',
            'permissions' => 'required'
        ]);

        $permission_id = $this->permissionRepository->getId($input['permissions']);

        $input['permissions'] = $permission_id;

        $this->rolePermissionRepository->assignPermissions($input);

        return $this->httpOK([
            'data'=>[
                'permissions' => $this->rolePermissionRepository
                    ->getPermission($input['role_id'])
            ]
        ]);
    }
}