<?php


namespace App\Http\Controllers;


use App\Traits\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;

class ForgotPasswordController extends Controller
{
    use ResetsPasswords;

    /**
     * @param Request $request
     *
     * @return Response
     * @throws ValidationException
     */
    public function sendResetLinkEmail(Request $request)
    {
        $input = $this->validate($request,[
            'email' => 'required|email'
        ]);

        $broker = $this->getBroker();

        $response = Password::broker($broker)
            ->sendResetLink($request->only('email'));

        switch ($response) {
            case Password::RESET_LINK_SENT:
                return $this->getSendResetLinkEmailSuccessResponse($response);

            case Password::INVALID_USER:
            default:
                return $this->getSendResetLinkEmailFailureResponse($response);
        }
    }

}