<?php

namespace App\Http\Controllers;

use App\Repository\Permission\PermissionInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

class PermissionController extends Controller
{
    /**
     * @var PermissionInterface
     */
    private PermissionInterface $permissionRepository;

    /**
     * PermissionController constructor.
     *
     * @param PermissionInterface $repository
     */
    public function __construct(PermissionInterface $repository)
    {
        $this->permissionRepository = $repository;
    }

    /**
     * @return JsonResponse
     */
    public function generate()
    {
        $routes = $this->moduleRoutes();

        foreach ($routes as $moduleName => $moduleRoutes) {

            if ($moduleName === '') {
                continue;
            }

            foreach ($moduleRoutes as $permissionName) {
                if ($permissionName['permissionName'] === '') {
                    continue;
                }

                $permissions[] = $permissionName['permissionName'];
                $modules[] = $moduleName;

                $input = [
                    'module' => $moduleName,
                    'permission' => $permissionName['permissionName'],
                ];

                $this->permissionRepository->create($input);
            }
            $this->permissionRepository->deletePermission($moduleName, $permissions);
        }

        $this->permissionRepository->deleteModule($modules);

        return $this->httpOK([
            'data' => [
                'permissions' => $this->getAllPermissions(),
            ],
        ]);
    }

    /**
     * @return Collection
     */
    public function moduleRoutes(): Collection
    {
        return collect(app('router')->getRoutes())
            ->map(function ($route) {
                return [
                    'module' => $this->getModuleName($route),
                    'permissionName' => $this->getRouteName($route),
                ];
            })
            ->groupBy('module');
    }

    /**
     * @param $route
     *
     * @return string
     */
    public function getModuleName($route): string
    {
        if (!isset($route['action']['middleware'])) {
            return '';
        }

        if (is_string($route['action']['middleware'])) {
            $middleware = explode(':', $route['action']['middleware']);
            if ($middleware[0] === 'permission') {
                return $middleware[1] ?? '';
            }
        }

        if (is_array($route['action']['middleware'])) {
            foreach ($route['action']['middleware'] as $middleware) {
                $middleware = explode(':', $middleware);
                if ($middleware[0] === 'permission') {
                    return $middleware[1] ?? '';
                }
            }
        }

        return '';
    }

    /**
     * @param $route
     *
     * @return string
     */
    public function getRouteName($route): string
    {
        return $route['action']['as'] ?? '';
    }

    /**
     * @return JsonResponse
     */
    public function getAllPermissions(): JsonResponse
    {
        //TODO: Create Permission Repository
        return $this->httpOk([
            'data' => [
                'permissions' => $this->permissionRepository->listAll(),
            ],
        ]);
    }
}

