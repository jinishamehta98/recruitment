<?php

namespace App\Http\Controllers;

use App\Http\Resource\UserResource;
use App\Models\User;
use App\Repository\User\UserInterface;
use Dusterio\LumenPassport\Http\Controllers\AccessTokenController;
use GuzzleHttp\Psr7\BufferStream;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Symfony\Bridge\PsrHttpMessage\Tests\Fixtures\ServerRequest;

class AuthController extends Controller
{
    /**
     * @var UserInterface
     */
    protected UserInterface $userRepository;

    /**
     * AuthController constructor.
     *
     * @param UserInterface $repository
     */
    public function __construct(UserInterface $repository)
    {
        $this->userRepository = $repository;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function register(Request $request): JsonResponse
    {
        $input = $this->validate($request, [
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|unique:users',
            'contact_no' => 'required|regex:/[0-9]{10}/',
            'password' => 'required',
            'profile_pic' => 'sometimes|image',
        ]);

        return $this->httpOK([
            'data' => [
                'user' => new UserResource(
                    $this->userRepository->register($input)
                ),
            ],
            'message' => 'user registered',
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function login(Request $request)
    {
        $input = $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        $user = User::where('email', $input['email'])->first();

        if (!$user) {
            return $this->httpOK([
                'message' => 'Invalid Username/Email',
            ],401);
        }

        $response = app(AccessTokenController::class)
            ->issueToken(
                new ServerRequest(
                    '1.1',
                    [],
                    new BufferStream(''),
                    '/oauth/token',
                    'POST',
                    config('app.url') . '/oauth/token',
                    [],
                    [],
                    ['url' => config('app.url')],
                    [],
                    [
                        'client_id' => config('api.password_client_id'),
                        'client_secret' => config('api.password_client_key'),
                        'username' => $input['email'],
                        'password' => $input['password'],
                        'remember' => true,
                        'scope' => '',
                        'grant_type' => 'password',
                    ],
                    []
                )
            );

        return $this->httpOK([
            'data' => json_decode($response->getBody()),
        ]);
    }

    /**
     * @return Authenticatable
     */
    public function logout(): Authenticatable
    {
        return User::authLogout();
    }
}
