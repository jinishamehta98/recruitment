<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use Laravel\Lumen\Routing\Controller as BaseController;

class Controller extends BaseController
{
    /**
     * @param array $response
     * @param int   $code
     *
     * @return JsonResponse
     */
    public function httpOK(array $response = [], $code = 200)
    {
        $response['code'] = $code;
        $response['data'] = $response['data'] ?? [];

        if (!isset($response['status']) && $response['code'] === 200) {
            $response['status'] = true;
        }

        return response()->json($response, $code);
    }
}
