<?php

namespace App\Http\Controllers;

use App\Repository\User\UserInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * @var UserInterface
     */
    private UserInterface $userRepository;

    /**
     * UserController constructor.
     *
     * @param UserInterface $repository
     */
    public function __construct(UserInterface $repository)
    {
        $this->userRepository = $repository;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function update(Request $request)
    {
        $input = $this->validate($request, [
            'first_name' => 'sometimes|max:255',
            'last_name' => 'sometimes|max:255',
            'contact_no' => 'sometimes|regex:/[0-9]{10}/',
            'profile_pic' => 'sometimes|image',
        ]);

        $userId = Auth::user()->id;

        return $this->httpOK([
            'data' => [
                'user' => $this->userRepository->update($input, $userId),
            ],
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function view()
    {
        $userId = Auth::user()->id;

        return $this->httpOK([
            'data' => [
                'user' => $this->userRepository->view($userId),
            ],
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function changePassword(Request $request){

        $input = $this->validate($request,[
            'oldPassword' => 'required',
            'newPassword' => 'required'
        ]);

        return $this->httpOK([
            'data' => [
                'user' => $this->userRepository->changePassword($input)
            ]
        ]);

    }
}