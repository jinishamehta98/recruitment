<?php
//TODO :: Clear doubt about skill management

namespace App\Http\Controllers;


use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class SkillController extends Controller
{
    /**
     * @var SkillInterface
     */
    private SkillInterface $skillRepository;

    /**
     * SkillController constructor.
     *
     * @param SkillInterface $repository
     */
    public function __construct(SkillInterface $repository)
    {
        $this->skillRepository = $repository;
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createSkill(Request $request){
        $input = $this->validate($request,[
            'skill' => 'required|max:255'
        ]);

        return $this->httpOK([
            'data' => [
                'skills' => $this->skillRepository->create($input)
            ]
        ]);
    }

    /**
     * @param Request $request
     *
     * @return JsonResponse
     * @throws ValidationException
     */
    public function createSkills(Request $request){
        $input = $this->validate($request,[
            'skills.*.skill' => 'required|max:255'
        ]);

        return $this->httpOK([
            'data' => [
                'skills' => $this->skillRepository->createBulk($input)
            ]
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function listAllSkill(){
        return $this->httpOK([
            'data' => [
                'skills' => $this->skillRepository->listAll()
            ]
        ]);
    }

    /**
     * @param int $skillId
     *
     * @return JsonResponse
     */
    public function deleteSkill(int $skillId){
        return $this->httpOK([
            'data' => [
                'message' => $this->skillRepository->delete($skillId)
            ]
        ]);
    }

    /**
     * @param int $skillId
     *
     * @return JsonResponse
     */
    public function updateSkill(int $skillId){
        return $this->httpOK([
            'data' => [
                'skill' => $this->skillRepository->update($skillId)
            ]
        ]);
    }

}