<?php

namespace App\Http\Middleware;

use Closure;

class PermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */


    public function handle($request, Closure $next)
    {
//        return $next($request);

        if (!$this->isPermissionDenied($request)) {
            return response()->json([
                'code' => 401,
                'message' => __('permission denied'),
                'data' => null,
            ]);
        }

        return $next($request);

    }

    public function isPermissionDenied($request)
    {
        try {
            $route = $request->route();
            if (!isset($route[1]['as'])) {
                return false;
            }

            $routeName = $route[1]['as'];
            $middlewares = $route[1]['middleware'];

            if (count($middlewares) === 0) {
                return false;
            }

            $user = $request->user();
            $permissions = $user->getAllPermissions();

            foreach ($middlewares as $key => $middleware) {
                $arr = explode(':', $middleware);


                if($arr[0] === 'permission' && $permissions->where('name', $routeName)->count() > 0) {
                    return true;
                }
            }

            return false;
        } catch (Exception $e) {
            return [false];
        }
    }
}



