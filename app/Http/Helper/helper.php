<?php

if (!function_exists('generatePassword')) {

    /**
     * @param int $length
     *
     * @return string
     */
    function generatePassword(int $length = 8)
    {
        if (!isset($password)) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $password = '';
            for ($i = 0; $i < $length; $i++) {
                $password .= $characters[rand(0, $charactersLength - 1)];
            }
        }
        return $password;
    }
}