<?php
//TODO :: Add Create User Mailing Functionality
namespace App\Events;

use App\Models\User;

class CreateUserEmailEvent extends Event
{
    /**
     * @var User
     */
    public User $user;
    /**
     * @var string
     */
    private string $password;

    /**
     * CreateUserEmailEvent constructor.
     *
     * @param User   $user
     * @param string $password
     */
    public function __construct(User $user,string $password)
    {
        $this->user = $user;
        $this->password = $password;
    }
}