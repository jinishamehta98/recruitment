<?php

namespace App\Events;

use App\Models\User;

class SendPasswordResetMailEvent extends Event
{
    /**
     * @var User
     */
    public User $user;
    /**
     * @var string
     */
    public string $token;

    /**
     * SendPasswordResetMailEvent constructor.
     *
     * @param User   $user
     * @param string $token
     */
    public function __construct(User $user, string $token)
    {
        $this->user = $user;
        $this->token = $token;
    }
}