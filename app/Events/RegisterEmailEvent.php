<?php

namespace App\Events;

use App\Models\User;

class RegisterEmailEvent extends Event
{
    /**
     * @var User
     */
    public User $user;

    /**
     * RegisterEmailEvent constructor.
     *
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }
}