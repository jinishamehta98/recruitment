<?php

namespace App\Mail;

use App\Events\Event;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RegisterEmailMail extends Mailable
{
    use Queueable, SerializesModels;

    private User $user;

    /**
     * RegisterEmailMail constructor.
     *
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->user = $event->user;
        $this->to($this->user->email);
    }

    /**
     * @return RegisterEmailMail
     */
    public function build(){

        return $this->markdown('register_email',[
            'user' => $this->user,
            'url' => config('app.url')
        ]);
    }
}