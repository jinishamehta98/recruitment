<?php


namespace App\Mail;


use App\Events\Event;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendPasswordResetMail extends Mailable
{
    use Queueable, SerializesModels;

    private User $user;
    private $token;

    /**
     * RegisterEmailMail constructor.
     *
     * @param Event $event
     */
    public function __construct(Event $event)
    {
        $this->user = $event->user;
        $this->to($this->user->email);
        $this->token = $event->token;
    }

    /**
     * @return SendPasswordResetMail
     */
    public function build()
    {
        $url = config('app.url').'/password/reset/'.$this->token;
        return $this->markdown('password_reset_email', [
            'user' => $this->user,
            'url' => $url,
            'token' => $this->token
        ]);
    }
}