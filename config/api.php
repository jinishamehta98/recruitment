<?php

return [
    'personal_client_id' => env('PERSONAL_CLIENT_1_ID', '1'),
    'personal_client_key' => env('PERSONAL_CLIENT_1_SECRET', ''),

    'password_client_id' => env('PASSWORD_CLIENT_2_ID', '2'),
    'password_client_key' => env('PASSWORD_CLIENT_2_SECRET', ''),
];
