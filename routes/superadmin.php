<?php

/** @var Router $router */

use Laravel\Lumen\Routing\Router;

$router->group([
    'prefix' => 'superAdmin',
    'as' => 'superAdmin',
    'middleware' => [
        'auth',
    ],
], function ($router) {

    $router->group([
        'namespace' => 'SuperAdmin',
    ], function ($router) {
        $router->group([
            'prefix' => 'role',
            'middleware' => [
                'permission:role',
            ],
            'as' => 'role',
        ], function ($router) {

            $router->get('list', [
                'uses' => 'RoleController@listRole',
                'as' => 'list',
            ]);

            $router->post('create', [
                'uses' => 'RoleController@createRole',
                'as' => 'create',
            ]);

            $router->delete('delete', [
                'uses' => 'RoleController@deleteRole',
                'as' => 'delete',
            ]);

            $router->post('createBulk', [
                'uses' => 'RoleController@createRoles',
                'as' => 'createBulk',
            ]);
        });

        $router->group([
            'prefix' => 'user',
            'middleware' => [
                'permission:user',
            ],
            'as' => 'user',
        ], function ($router) {

            $router->get('list', [
                'uses' => 'UserController@listUser',
                'as' => 'list',
            ]);

            $router->get('view/{userId}', [
                'uses' => 'UserController@viewUser',
                'as' => 'detail',
            ]);

            $router->post('update/{userId}', [
                'uses' => 'UserController@updateUser',
                'as' => 'update',
            ]);

            $router->post('create', [
                'uses' => 'UserController@createUser',
                'as' => 'create',
            ]);

            $router->delete('delete/{userId}', [
                'uses' => 'UserController@deleteUser',
                'as' => 'delete',
            ]);
        });

        $router->group([
            'prefix' => 'permission',
            'middleware' => [
                'permission:permission',
            ],
            'as' => 'permission',
        ], function ($router) {

            $router->post('assign', [
                'uses' => 'RolePermissionController@assignPermissionToRole',
                'as' => 'assign',
            ]);
        });
    });

    $router->group([
        'prefix' => 'permission',
        'middleware' => [
            'permission:permission',
        ],
        'as' => 'permission',
    ], function ($router) {
        $router->get('list', [
            'uses' => 'PermissionController@getAllPermissions',
            'as' => 'list',
        ]);
    });
});
