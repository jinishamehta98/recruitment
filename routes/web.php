<?php

/** @var Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Laravel\Lumen\Routing\Router;

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group([
    'prefix' => 'api',
], function ($router) {

    require 'superadmin.php';

    $router->post('signup', [
        'uses' => 'AuthController@register',
        'as' => 'user.signup',
    ]);

    $router->post('login', [
        'uses' => 'AuthController@login',
        'as' => 'user.login',
    ]);

    $router->post('/password/email',[
        'uses' => 'ForgotPasswordController@sendResetLinkEmail',
        'as' => 'forgotPassword'
    ]);

    $router->post('/password/reset',[
        'uses' => 'ResetPasswordController@reset',
        'as' => 'resetPassword'
    ]);

    $router->group([
        'middleware' => [
            'auth'
        ]
    ],function($router){
        $router->get('logout', [
            'uses' => 'AuthController@logout',
            'as' => 'user.logout',
        ]);

        $router->get('view', [
            'uses' => 'UserController@view',
            'as' => 'user.view',
        ]);

        $router->post('update', [
            'uses' => 'UserController@update',
            'as' => 'user.update',
        ]);

        $router->post('changePassword', [
            'uses' => 'UserController@changePassword',
            'as' => 'user.changePassword',
        ]);
    });

    $router->get('generate',[
        'uses' => 'PermissionController@generate',
        'as' => 'permission.generate'
    ]);
});
