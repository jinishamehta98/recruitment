<html>
<head>
</head>
<body>

@component('mail::message')
# Hello {{ $user->username }}
Welcome to {{ config('app.name') }}.
{{__('You requested for password reset') }}

{{__('Click below link to reset your password') }}
{{$token}}
@component('mail::button', ['url' => $url])
{{('password reset')}}
@endcomponent

{{ __('Thanks')}},<br>
{{ config('app.name') }}
@endcomponent

</body>
</html>
