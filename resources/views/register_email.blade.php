<html>
<head>
</head>
<body>

@component('mail::message')
# Hello {{ $user->username }}
Welcome to {{ config('app.name') }}.
{{__('Thank you for your registering!') }}

{{__('To explore More about the website click below') }}

@component('mail::button', ['url' => $url])
{{config('app.name')}}
@endcomponent

{{ __('Thanks')}},<br>
{{ config('app.name') }}
@endcomponent

</body>
</html>
