<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobOpeningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('job_opening', function (Blueprint $table) {
            $table->unsignedBigInteger('id')->autoIncrement();
            $table->string('post_id')->unique();
            $table->unsignedInteger('job_category_id');
            $table->unsignedInteger('job_position_id');
            $table->unsignedInteger('vacancy');
            $table->string('required_experience')->nullable();
            $table->string('other_requirements')->nullable();
            $table->string('description')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->timestamp('date_open');
            $table->timestamp('date_close')->nullable();
            $table->timestamp('target_date_close')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('job_opening');
    }
}
